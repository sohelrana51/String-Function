<!DOCTYPE html>
<html>
<body>

<?php
echo strip_tags("Hello <b>world!</b>");
?>

<p>This function strips a string from HTML, XML, and PHP tags. In this example, the &lt;b&gt; tag gets stripped.</p>

</body>
</html>